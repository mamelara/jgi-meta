#!/usr/bin/env python

import sys, os
#sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib


#requires an active aws cli if using s3
'''
args
- s3 or local staging dir with default
- time check argument
- outputdir for final files
- send only flag
'''
if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

VERSION = "1.0.0"
def main():
    '''get all failures get date input file and output dir'''
    url = subprocess.Popen('~/scripts/cromwell_server.bash', shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8').rstrip()
    cmd = 'curl --silent -X GET "http://' + url + '/api/workflows/v1/query?status=Failed" -H  "accept: application/json"'

    failed = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8'))
    for result in failed['results']:
        cmd = 'curl --silent -X GET "http://' + url + '/api/workflows/v1/' + result['id'] + '/metadata?expandSubWorkflows=false" -H  "accept: application/json"'
        job = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8'))
        hours = timediff(time2=result['end'])
        if hours < (-1*(24*14)) and hours > (-1*(24*30)):
            pass
        else:
            continue
        if  job['submittedFiles']['inputs'].find('jgi_meta.input_file') != -1:
            inputs_json = json.loads(job['submittedFiles']['inputs'])
            input_file = inputs_json['jgi_meta.input_file']
            try:
                stdout = os.path.dirname(os.path.dirname(job['calls'][list(job['calls'].keys())[0]][0]['stdout']))
            except:
                stdout = ""

            print("aws s3 rm " + input_file)
            if stdout:
                print("aws s3 rm " + stdout + " --recursive")



    sys.exit()
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-j", "--inputsjson", required=True, help="inputs.json with {{input_file}}\n")
    parser.add_argument("-w", "--wdl", required=True, help="wdl file for cromwell.\n")
    parser.add_argument("-s", "--s3", required=False,default='s3://bf-20190529-uswest2-s3/cromwell-execution', help="s3 destination for input file/n")
    parser.add_argument("-c", "--cromwellhost", required=False,default="127.0.0.1:8000",  help="cromwell host to push job to\n")

    parser.add_argument("-l", "--live", required=False, default=False, action='store_true', help="actually run else pring commands\n")    
    parser.add_argument("-t", "--time", required=False, default=30, help="time in seconds to monitor the directory. (default=60)\n")
    parser.add_argument("-o", "--outputdir", required=False, default = './', help="outputdir for outputs. default=send to cromwell only\n")
    parser.add_argument("-q", "--quiet", required=False, default=False,action='store_true', help="quiets command printing")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()
        
    if not args.s3.startswith('s3://'):
        sys.stderr.write(args.s3 + " does not look like s3://\n")
        sys.exit(1)
    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    args.outputdir == os.path.abspath(args.outputdir)
    
    if args.live:
        try:
            cmd = ['curl', '-Is', 'http://' + args.cromwellhost ]
            print(cmd)
            if not args.quiet:sys.stdout.write(" ".join(cmd) + "\n")
            retcode = subprocess.check_output(cmd)
        except  e:
            print(e)
            sys.stderr.write(args.cromwellhost + " does not appear up \n")
            sys.exit(1)

        
    #get workflow name frow wdl
    workflow = ""
    with open(args.wdl,"r") as f:
        for line in f.readlines():
            if line.startswith("workflow"):
                workflow = line.split()[1]
                workflow = re.sub(r"{",r'',workflow)
                workflow = re.sub(r'\s',r'',workflow)

    with open(args.inputsjson, "r") as f:inputs_json = json.loads(f.read())
    inputs_tag = workflow + ".input_file"
    inputs_file_local = inputs_json[inputs_tag]

    m = hashlib.md5()
    m.update(str(inputs_file_local + str(time.time())).encode('utf-8'))
    ts= m.hexdigest() + "_._"

    inputs_file_s3 = os.path.join(args.s3,ts + os.path.basename(inputs_file_local))
    inputs_json[inputs_tag] = inputs_file_s3
    final_inputs_json = os.path.join(args.outputdir, ts + "inputs.json")
    with open(final_inputs_json,"w") as f:f.write(json.dumps(inputs_json,indent=3))

    try:
        cmd = ["aws","s3", "cp", inputs_file_local, inputs_file_s3]
        if not args.quiet:sys.stdout.write(" ".join(cmd) + "\n")
        if args.live:retcode = subprocess.check_output(cmd)
    except:
        sys.stderr.write("aws copy not successful")
        sys.exit(1)

    cmd = 'curl --silent -X POST "http://' + args.cromwellhost + '/api/workflows/v1" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" '
    cmd += '-F "workflowSource=@' + args.wdl + ';" ' 
    cmd += '-F "workflowInputs=@' + final_inputs_json + ';type=application/json" '
#    cmd += '-F "workflowOptions=@options.json;type=application/json" '
    if not args.quiet:sys.stdout.write(cmd + "\n") 
    if not args.live:sys.exit(0)

    json_sub_text = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    submission = json.loads(json_sub_text)
    sub_id = submission['id']
    if not args.quiet:sys.stdout.write("id=" + sub_id + "\n")
    finished = False
    
    cmd = 'curl --silent -X GET "http://' + args.cromwellhost + '/api/workflows/v1/' + sub_id + '/metadata?expandSubWorkflows=false" -H  "accept: application/json"'
    if not args.quiet:sys.stdout.write(cmd + "\n")
    metadatafile = os.path.join(args.outputdir,ts + "cromwell.metadata")
    while not finished:
        time.sleep(int(args.time))
        metadata = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())
        if not args.quiet:
            sys.stdout.write(metadata['status'] + "\n")
            sys.stdout.flush()
        with open(metadatafile,"w") as f:
            f.write(json.dumps(metadata,indent=3))
        #['Running', 'Succeeded', 'Aborted', 'Submitted', 'On Hold', 'Failed']
        if metadata['status'] not in ['Submitted', 'Running', 'On Hold']:
            finished = True

    if metadata['status'] in ['Aborted', 'Failed']:
        sys.stderr.write("Error, check on: " + metadata['id'] + "\n")
        sys.exit(1)

    if len(metadata['outputs']):
        remote_s3_dir = copy_outputs(metadata,ts, args.outputdir, args.quiet)
        cmd = ["aws","s3", "rm", remote_s3_dir, "--recursive"]
        if not args.quiet:sys.stdout.write(" ".join(cmd) + "\n")
        retcode = subprocess.check_output(cmd)
    else:
        sys.stderr.write("Error no outputs found in cromwell metadata")
        sys.exit(1)

    cmd = ["aws","s3", "rm", inputs_file_s3]
    if not args.quiet:sys.stdout.write(" ".join(cmd) + "\n")
    retcode = subprocess.check_output(cmd)
        
    sys.stdout.write("Done\n")
    sys.stdout.write("Done\n")
    sys.exit()

def copy_outputs(metadata, prefix, localdir, quiet):
    '''takes cromwell metadata, finds the first s3 output and cps it locally with prefix                                                                                                               
    if successfull, returns the s3 dir for deletion'''

    s3_dir = ""
    cromwellid = metadata['id']
    localdir_root= os.path.join(localdir,prefix + cromwellid)

    #to add, check for outputs. If not, then get source path from "calls"                                                                                                                              
    try:
        for key in sorted(metadata['outputs']):
            if s3_dir:
                continue
            source_path = metadata['outputs'][key]
            print (str(source_path))
            if not str(source_path).startswith('s3://'):
                continue
            if source_path.find('/call-') != -1 and source_path.find('/cromwell-execution/') != -1:
                s3_dir = re.sub(r'^(.*?/)call-.*$',r'\1',source_path)
            cmd = ["aws","s3", "cp", s3_dir, localdir_root, "--recursive"]
            if not quiet:sys.stdout.write(" ".join(cmd))
            procExe = subprocess.Popen(cmd , stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
            while procExe.poll() is None:
                line = procExe.stdout.readline()
                if line.startswith("download:"):
                    sys.stdout.write(line)
    except Exception as e:
        sys.stderr.write(e)
        return
    return s3_dir

def timediff (time1="", time2=""):
    '''time diff time2 required (default = now)'''
    import dateutil.parser
    from pytz import timezone
    import datetime

    if not time2:
        sys.exit("time2 required")
    else:
        ts2 = str(time2)
    if not time1:
        ts1 = str(datetime.datetime.now())
    else:
        ts1 = str(time1)

    pst = timezone('US/Pacific')
    ts1_ = dateutil.parser.parse(ts1)
    ts2_ = dateutil.parser.parse(ts2)
    if ts1_.tzinfo == None:
        ts1_ = pst.localize(ts1_)
    if ts2_.tzinfo == None:
        ts2_ = pst.localize(ts2_)
    diff = ts2_ - ts1_
    return diff.total_seconds()/60**2 # hours


if __name__ == '__main__':
    main()
