#!/usr/bin/env bash

set -eo pipefail

curl --silent --fail -o min.sh -L https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && bash min.sh -b -p $PWD/miniconda3 && rm min.sh && cd $PWD/miniconda3/bin 

./conda config --add channels defaults
./conda config --add channels conda-forge
./conda config --add channels bioconda
./conda install -y pyaml samtools=1.5
cd $PWD
