#!/usr/bin/env bash

set -eo pipefail

source /global/projectb/sandbox/gaag/bfoster/eclipse2/miniconda3/etc/profile.d/conda.sh && conda activate
export PATH=/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/jgi-meta/bin:$PATH
export dirname=$1
[ -d $dirname ] || exit 1

cd $dirname;
export logfile="status.log"
export file=$(head -1  call-bbcms/bbcms-stderr.log |  sed s/^.*in=// |  cut -f 1 -d " " |  sed s/^.*_._//)


echo "Getting assembly stats, "$(date +%Y-%m-%d;date +%H:%M:%S) >> $logfile
mkdir -p call-assembly_stats;
shifter --image=bryce911/bbtools:latest -- stats.sh in=call-create_agp/assembly.scaffolds.fasta 1>| call-assembly_stats/assembly.scaffolds.fasta.stats.txt 2>| call-assembly_stats/assembly.scaffolds.fasta.stats.txt.e ;
shifter --image=bryce911/bbtools:latest -- stats.sh format=6 in=call-create_agp/assembly.scaffolds.fasta 1>| call-assembly_stats/assembly.scaffolds.fasta.stats.tsv 2>| call-assembly_stats/assembly.scaffolds.fasta.stats.tsv.e ;

echo "Getting metadata, "$(date +%Y-%m-%d;date +%H:%M:%S) >> $logfile
pre_meta_assembly.py -i $file 1> setup.json 2> setup.json.e
echo "Creating reports, "$(date +%Y-%m-%d;date +%H:%M:%S) >> $logfile
report_metag_bbcms_spades.py -i $PWD -o report -j setup.json --cromwell 1> report.o 2> report.e 
echo "Finalizing for submission, "$(date +%Y-%m-%d;date +%H:%M:%S) >> $logfile
post_meta_assembly.py -i $PWD -o report -j report/metadata.json --cromwell 1>> report.o 2>> report.e || post_meta_assembly.py -i $PWD -o report -j report/metadata.json --cromwell --no_rqc_pdf 1>> report.o 2>> report.e 
echo "pipeline Completed successfully, "$(date +%Y-%m-%d;date +%H:%M:%S) >> $logfile






