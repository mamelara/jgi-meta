workflow jgi_meta {
    String input_file
    String mem_requested
    String num_threads
    String bbtools_container="bryce911/bbtools:38.44"
    String spades_container="bryce911/spades:3.13.0"
    call head {
      	 input: files=input_file
    }
    call readstats as readstats_raw {
      	 input: infile=head.out,container=bbtools_container
    }
    call bbcms {
      	 input: infile=head.out,mem=mem_requested,threads=num_threads, container=bbtools_container
    }
    call readstats as readstats_bbcms {
      	 input: infile=bbcms.out,container=bbtools_container
    }
    call split {
      	 input: infile=bbcms.out,container=bbtools_container
    }
    call assy {
      	 input: infile1=split.out1, infile2=split.out2,mem=mem_requested,threads=num_threads, container=spades_container
    }
    call create_agp {
    	  input:scaffolds_in=assy.out,container=bbtools_container
    }
    call assembly_stats {
    	  input:scaffolds=create_agp.scaffolds_out,container=bbtools_container
    }
    call read_mapping_pairs{
    	 input:reads=head.out,ref=create_agp.contigs_out,mem=mem_requested,threads=num_threads, container=bbtools_container
    }
}
task head {
	String files
	String outfile="out.fastq.gz"
	command{
		cat ${files} > ${outfile}
	}
	output {
	  	File out = outfile
	}
}

task readstats {
     File infile
     String container
     String outfile="readlen.txt"
     runtime {
     	     docker: container
     }
     command {
     	     readlength.sh in=${infile} 1>| ${outfile}
     }
     output {
     	    File out = outfile
     }
}

task bbcms {
     File infile
     String container
     String mem
     String threads 
     String outfile="input.corr.fastq.gz"
     String counts="counts.metadata.json"
     runtime {
     	     docker: container
     }
     command {
	     bbcms.sh threads=${threads} -Xmx${mem}g metadatafile=${counts} mincount=2 highcountfraction=0.6  in=${infile} out=${outfile}
     }
     output {
     	    File out = outfile
	    File outcounts = counts
     }
}
	     

task split{
     File infile
     String container
     String outpath1="reads1.fasta"
     String outpath2="reads2.fasta"
     String counts="counts.metadata.json"
     runtime {
     	     docker: container
     }
     command {
	     reformat.sh metadatafile=${counts} overwrite=true in=${infile} out=${outpath1} out2=${outpath2}
     }
     output {
     	    File out1 = outpath1
     	    File out2 = outpath2
	    File outcounts = counts
     }
}


task assy {
     File infile1
     File infile2
     String container
     String mem
     String threads 
     String outprefix="spades3"
     String outfile="${outprefix}/scaffolds.fasta"
     String spadeslog ="${outprefix}/spades.log"
     runtime {
     	     docker: container
     }
     command{
	unset OMP_NUM_THREADS
        spades.py -m ${mem} -o ${outprefix} --tmp-dir /tmp --only-assembler -k 33,55,77,99,127 --meta -t ${threads} -1 ${infile1} -2 ${infile2}
	}
     output {
     	    File out = outfile
     	    File outlog = spadeslog
     } 
}

task create_agp {
  File scaffolds_in
  String container
  String prefix="assembly"
  String contigs="${prefix}.contigs.fasta"
  String scaffolds="${prefix}.scaffolds.fasta"
  String agp="${prefix}.agp"
  String legend="${prefix}.scaffolds.legend"
  runtime {
  	  docker: container
  }
  command{
    	fungalrelease.sh -Xmx10g in=${scaffolds_in} out=${scaffolds} outc=${contigs} agp=${agp} legend=${legend} mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
  }
  output{
    File contigs_out = contigs
    File scaffolds_out = scaffolds
    File agp_out = agp
    File legend_out = legend
  }
}

task assembly_stats{
  File scaffolds
  String container
  String tsv="summary.tsv"
  String txt="summary.txt"
  runtime {
  	  docker: container
  }  
  command{
    stats.sh format=6 in=${scaffolds} 1>| ${tsv}
    stats.sh in=${scaffolds} 1>| ${txt}
  }
  output{
    File tsv_out = tsv
    File txt_out = txt
  }
}


task read_mapping_pairs{
  File reads
  File ref
  String container
  String mem
  String threads 
  String unsorted="pairedMapped.bam"
  String outsam="pairedMapped.sam.gz"
  String sorted="pairedMapped_sorted.bam"
  String sorted_idx="pairedMapped_sorted.bam.bai"
  String bamscript="to_bam.sh"
  String cov="covstats.txt"
  runtime {
  	  docker: container
  }  
  command{
      bbmap.sh threads=${threads} -Xmx${mem}g nodisk=true interleaved=true ambiguous=random in=${reads} ref=${ref} out=${unsorted} covstats=${cov} bamscript=${bamscript}
      samtools sort -m100M -@ ${threads} ${unsorted} -o ${sorted}
      samtools index ${sorted}
      reformat.sh threads=${threads} -Xmx${mem}g in=${unsorted} out=${outsam} overwrite=true
      ln -sf stderr mapping.bash.e
  }
  output{
      File bamfile_ = sorted
      File bamfile_idx_out = sorted_idx
      File covfile_out = cov
      File samfile_out = outsam
  }
}

