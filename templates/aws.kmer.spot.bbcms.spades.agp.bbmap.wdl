workflow jgi_meta {
    File input_file
    String unique_kmer
    String bbtools_container="bryce911/bbtools:38.44"
    String spades_container="bryce911/spades:3.13.0"
    call bbcms {
      	 input: infile=input_file, uniquekmer=unique_kmer, container=bbtools_container
    }
    call assy { 
    	 input: infile1=bbcms.out1, infile2=bbcms.out2, mempred=bbcms.predicted_mem, container=spades_container 
    }
    call create_agp {
    	  input:scaffolds_in=assy.out, container=bbtools_container
    }
    call read_mapping_pairs{
    	 input:reads=input_file,ref=create_agp.outcontigs, total_ram=create_agp.out_total_ram, java_ram=create_agp.out_java_ram, container=bbtools_container
    }
    output {
   	 File bbcms_out = bbcms.out
   	 File bbcms_out1 = bbcms.out1
   	 File bbcms_out2 = bbcms.out2
     	 File bbcms_stdout = bbcms.stdout
     	 File bbcms_stderr = bbcms.stderr
	 File bbcms_counts = bbcms.outcounts
	 File bbcms_kmer = bbcms.outkmer

	 File spades_log = assy.outlog
	 File spades_assy = assy.out

	 File contigs = create_agp.outcontigs
	 File scaffolds = create_agp.outscaffolds
	 File agp = create_agp.outagp

	 File bam = read_mapping_pairs.outbamfile
	 File bamidx = read_mapping_pairs.outbamfileidx
	 File sam = read_mapping_pairs.outsamfile
	 File cov = read_mapping_pairs.outcovfile

	 File bbcms_resource = bbcms.outresources
	 File assy_resource = assy.outresources
	 File agp_resource = create_agp.outresources
	 File mapping_resource = read_mapping_pairs.outresources
    }    
}

#    AWS_uswest2-optimal-ceq
#    AWS_uswest2-m4-2xlarge-8c-32g-ceq
#    AWS_uswest2-m4-10xlarge-40c-160g-ceq

task bbcms {
     File infile
     Float uniquekmer
     String container

     #shooting for 30% occupancy based on: occupancy = (unique kmers)/((RAM bytes*0.7)*8/12)
     Int bbcms_mem = ceil(( uniquekmer /0.138)/1000000000)
     Int runtime_mem = if bbcms_mem < 30 then 30 else bbcms_mem
     Int runtime_mem_frac = ceil(runtime_mem * 0.85)
     String runtime_mem_string = runtime_mem + ' GB'

     String resources="resources.log"
     String outfile="input.corr.fastq.gz"
     String outfile1="input.corr.left.fastq.gz"
     String outfile2="input.corr.right.fastq.gz"
     String readlen="readlen.txt"
     String outlog="stdout.log"
     String errlog="stderr.log"
     String kmerfile="unique31mer.txt"
     String counts="counts.metadata.json"
     String dollar="$"
     runtime {
     	     docker: container
	     backend:  "AWS_uswest2-optimal-spot-ceq"
	     memory: runtime_mem_string
     }
     command {
     	touch ${resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${resources} &
	sleep 30
	bbcms.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) -Xmx${runtime_mem_frac}g metadatafile=${counts} mincount=2 highcountfraction=0.6 in=${infile} out=${outfile} > >(tee -a ${outlog}) 2> >(tee -a ${errlog} >&2) && grep Unique ${errlog} | rev |  cut -f 1 | rev  > ${kmerfile}
	reformat.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) in=${outfile} out1=${outfile1} out2=${outfile2} 
	readlength.sh in=${outfile} out=${readlen}
     }
     output {
     	    File out = outfile
     	    File out1 = outfile1
     	    File out2 = outfile2
     	    File outreadlen = readlen
	    File stdout = outlog
	    File stderr = errlog
	    File outcounts = counts
	    File outresources = resources
	    File outkmer = kmerfile
	    Int predicted_mem = ceil((read_float(kmerfile) * 0.00000002962 + 16.3) * 1.05 )
     }
}


task assy {
     File infile1
     File infile2
     Int mempred
     String container
     String resources="resources.log"

     String cmd_threads="16"
     String outprefix="spades3"
     String outfile="${outprefix}/scaffolds.fasta"
     String spadeslog ="${outprefix}/spades.log"
     String dollar="$"
     runtime {
     	     docker: container
	     backend:  "AWS_uswest2-optimal-spot-ceq"
	     memory: if mempred < 30 then "30 GB" else mempred + ' GB' 
     }
     command{
	echo "MemPred:" ${mempred}
        touch ${resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${resources} &
	sleep 30
	ulimit -u unlimited
     	OMP_NUM_THREADS=${dollar}(grep "model name" /proc/cpuinfo | wc -l) spades.py -m 2000 -o ${outprefix} --only-assembler -k 33,55,77,99,127  --meta -t ${dollar}(grep "model name" /proc/cpuinfo | wc -l) -1 ${infile1} -2 ${infile2}
     }
     output {
     	    File out = outfile
     	    File outlog = spadeslog
	    File outresources = resources
     } 
}
     	#OMP_NUM_THREADS=${threads} spades.py -m ${mempred} -o ${outprefix} --only-assembler -k 33,55,77,99,127 --meta -t ${threads} --12 ${infile}

task create_agp {
  File scaffolds_in
  String container

  String resources="resources.log"
  String cmd_mem="30"
  String cmd_threads="8"
  String prefix="assembly"
  String contigs="${prefix}.contigs.fasta"
  String scaffolds="${prefix}.scaffolds.fasta"
  String agp="${prefix}.agp"
  String legend="${prefix}.scaffolds.legend"
  String java_ram="java_ram.txt"
  String total_ram="total_ram.txt"
  runtime {
  	  docker: container
	  backend: "AWS_uswest2-m4-2xlarge-8c-32g-ceq"
	  memory: "30 GB"
  }
  command{
        touch ${resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${resources} &
	sleep30
    	fungalrelease.sh -Xmx${cmd_mem}g in=${scaffolds_in} out=${scaffolds} outc=${contigs} agp=${agp} legend=${legend} mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
	stats.sh in=${contigs} k=13 |  tail -1  | cut -f 2 |  cut -f 3,4 -d " " | tee ${total_ram}
	stats.sh in=${contigs} k=13 |  tail -1  | cut -f 1 | rev | cut -f 2 -d " " | rev | tee ${java_ram}
  }
  output{
    File outcontigs = contigs
    File outscaffolds = scaffolds
    File outagp = agp
    File outlegend = legend
    File outresources = resources
    String out_total_ram= read_string(total_ram)    
    String out_java_ram= read_string(java_ram)    
  }
}

task read_mapping_pairs{
  File reads
  File ref
  String container
  String total_ram
  String java_ram

  String resources="resources.log"
  String cmd_mem="30"
  String cmd_threads="8"
  String unsorted="pairedMapped.bam"
  String outsam="pairedMapped.sam.gz"
  String sorted="pairedMapped_sorted.bam"
  String sorted_idx="pairedMapped_sorted.bam.bai"
  String bamscript="to_bam.sh"
  String cov="covstats.txt"
  String dollar="$"
  runtime {
  	  docker: container
          backend:  "AWS_uswest2-optimal-spot-ceq"
          memory: total_ram
  }  
  command{
      touch ${resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${resources} &
      sleep 30
      bbmap.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${java_ram}  nodisk=true interleaved=true ambiguous=random in=${reads} ref=${ref} out=${unsorted} covstats=${cov} bamscript=${bamscript}
      samtools sort -m100M -@ ${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${unsorted} -o ${sorted}
      samtools index ${sorted}
      reformat.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${java_ram} in=${unsorted} out=${outsam} overwrite=true
  }
  output{
      File outbamfile = sorted
      File outbamfileidx = sorted_idx
      File outcovfile = cov
      File outsamfile = outsam
      File outresources = resources
  }
}
