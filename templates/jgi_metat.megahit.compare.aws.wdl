workflow jgi_meta {
    File input_file
    String bbtools_container="bryce911/bbtools:38.44"
    String megahit_container_latest="vout/megahit:latest"
    String megahit_container_old="foster505/megahit:v1.1.2"

    call assy as latest_assy {
         input: infile12=input_file, container=megahit_container_latest
    }
#    call create_agp as latest_agp{
#         input: contigs_in=latest_assy.out, container=bbtools_container
#    }
   call stats as latest_stats{
         input: contigs_in=latest_assy.out, container=bbtools_container
    }
    call read_mapping_pairs as latest_mapping {
    	 input: reads=input_file, ref=latest_assy.out, container=bbtools_container
    }


    call assy as old_assy {
         input: infile12=input_file, container=megahit_container_old
    }
#    call create_agp as old_agp{
#         input: contigs_in=old_assy.out, container=bbtools_container
#    }
    call stats as old_stats{
         input: contigs_in=old_assy.out, container=bbtools_container
    }
    call read_mapping_pairs as old_mapping {
    	 input: reads=input_file, ref=old_assy.out, container=bbtools_container
    }


}


task read_mapping_pairs{
    File reads
    File ref
    String container
  

    String filename_resources="resources.log"
    String filename_unsorted="pairedMapped.bam"
    String filename_outsam="pairedMapped.sam.gz"
    String filename_sorted="pairedMapped_sorted.bam"
    String filename_sorted_idx="pairedMapped_sorted.bam.bai"
    String filename_bamscript="to_bam.sh"
    String filename_cov="covstats.txt"
    String dollar="$"
    #runtime { backend: "Local"}
     runtime {
             docker: container
             backend:  "AWS_uswest2-m4-2xlarge-8c-32g-ceq" 
             memory: "29.5 GiB"
	     cpu: 8
     }
    command{
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
        bbmap.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) -Xmx28g  nodisk=true interleaved=true ambiguous=random in=${reads} ref=${ref} out=${filename_unsorted} covstats=${filename_cov} bamscript=${filename_bamscript}
	samtools sort -m100M -@ ${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${filename_unsorted} -o ${filename_sorted}
        samtools index ${filename_sorted}
        reformat.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) -Xmx28g in=${filename_unsorted} out=${filename_outsam} overwrite=true
  }
  output{
      File outbamfile = filename_sorted
      File outbamfileidx = filename_sorted_idx
      File outcovfile = filename_cov
      File outsamfile = filename_outsam
      File outresources = filename_resources
  }
}

task create_agp {
    File contigs_in
    String container
    String java="-Xmx28g"
    String filename_resources="resources.log"
    String prefix="assembly"
    String filename_contigs="${prefix}.contigs.fasta"
    String filename_scaffolds="${prefix}.scaffolds.fasta"
    String filename_agp="${prefix}.agp"
    String filename_legend="${prefix}.scaffolds.legend"
#    runtime {backend: "Local"} 
     runtime {
             docker: container
             backend:  "AWS_uswest2-m4-2xlarge-8c-32g-ceq" 
             memory: "29.5 GiB"
	     cpu: 8
     }
    command{
        fungalrelease.sh in=${contigs_in} out=${filename_scaffolds} outc=${filename_contigs} agp=${filename_agp} legend=${filename_legend} mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
  }
    output{
	File outcontigs = filename_contigs
	File outscaffolds = filename_scaffolds
	File outagp = filename_agp
    	File outlegend = filename_legend
    	File outresources = filename_resources
    }
}

task stats {
    File contigs_in
    String container
    String java="-Xmx30g"
    String filename_resources="resources.log"
    String filename_stats_txt="stats.txt"
    String filename_stats_tsv="stats.tsv"
#    runtime {backend: "Local"} 
     runtime {
             docker: container
             backend:  "AWS_uswest2-m4-2xlarge-8c-32g-ceq" 
             memory: "29.5 GiB"
	     cpu: 8

     }
    command{
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
	stats.sh in=${contigs_in} 1> ${filename_stats_txt}
	stats.sh format=6 in=${contigs_in} 1> ${filename_stats_tsv}
  }
    output{
	File outstats = filename_stats_txt
	File outstats2 = filename_stats_tsv
    	File outresources = filename_resources
    }
}

task assy {
     File infile12
     String container

     String filename_resources="resources.log"
  
     String outprefix="out.megahit"
     String filename_outfile="${outprefix}/final.contigs.fa"
     String dollar="$"
#     runtime {backend: "Local"}
     runtime {
             docker: container
             backend:  "AWS_uswest2-m4-2xlarge-8c-32g-ceq" 
             memory: "29.5 GiB"
	     cpu: 8
     }
     command{
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
	megahit -t ${dollar}(grep "model name" /proc/cpuinfo | wc -l) --k-list  23,43,63,83,103,123 -m 28000000000 -o ${outprefix} --12 ${infile12}
     }
     output {
            File out = filename_outfile
            File outresources = filename_resources
     }
}

task get_queue {
    String container
    String request
    String outfile="mem.json"
#    runtime {backend: "Local"} 
     runtime {
             docker: container
             backend: "genomics2-r4-spot-ceq"
             memory: "29 GiB"
	     cpu: 4
     }
    command {
python <<CODE
import sys,json
request = int(${request})
#for r4 family
bins = [30, 60, 121, 243, 487]
cpus = [4, 8, 16, 32, 64]
val = [i for i in bins if request < i][0]
cpu  = cpus[bins.index(val)]
out=dict()
out['request']=request
out['mem'] = float(val) - 1.0
out['mem_safe'] = int(out['mem'] * 0.95)
out['mem'] = str(int(out['mem'])) + " GiB"
out['cpu'] = str(cpu)
out['java'] = '-Xmx' + str(out['mem_safe']) + 'g'
with open("${outfile}","w") as f:f.write(json.dumps(out,indent=3))
CODE
    }
    output {
           File out = outfile
    } 
}     
