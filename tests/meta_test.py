#!/usr/bin/env python

import sys, os
import glob
import subprocess
import argparse
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import report_utils as rep

'''This will run tests on analysis templates located in templates
1) get all analysis templates
2) get test set metag
3) run analysis on local, denovo-debug, cori-debug
4) Compare results, unit test

'''
DEBUG=False
VERSION = "1.0"
def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-o", "--outputdir", required=False, default=os.path.join(os.path.dirname(__file__),"..","tmp"),help="outputdir default = ../tmp.\n")
    parser.add_argument("-d", "--debug", required=False, default=False, action='store_true', help="debug.\n")
    parser.add_argument("-c", "--clean", required=False, default=False, action='store_true', help="clean tmpdir if needed.\n")
    args = parser.parse_args()
    if args.outputdir:
        tmp_dir = os.path.realpath(args.outputdir)
    else:
        tmp_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","tmp")
    #cleanup tmp
    if not(os.path.exists(tmp_dir)):
        os.mkdir(tmp_dir)
        #sys.exit(tmp_dir + " Does not exist")
    
    if args.clean:
        cmd = "rm -rf " + tmp_dir + "/*"
        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
        if(0):print(out)
    bindir = glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","bin"))[0]
    pre_meta = glob.glob(os.path.join(bindir,"pre_meta_assembly.py"))[0]
    datasets = rep.hasher()
    datasets['metag']['file']="/global/dna/dm_archive/rqc/analyses/AUTO-218419/12795.3.286435.ATAAGGCG-CGCCTTAT.filter-METAGENOME.fastq.gz"
    datasets['metag']['options']="--include_closed --force_latest --toy -o setup.json --templates"
    
    datasets['metat']['file']="/global/dna/dm_archive/rqc/analyses/AUTO-214641/12757.1.282872.GAATCCGA-TCGGATTC.filter-MTF.fastq.gz"
    datasets['metat']['options']="--include_closed --force_latest --toy -o setup.json --templates"    

    datasets['lowv']['file']="/global/dna/dm_archive/rqc/analyses/AUTO-219976/12798.2.287123.ATTCAGAA-AGGCGAAG.filter-VIRAL-METAGENOME.fastq.gz"
    datasets['lowv']['options']="--include_closed --force_latest --toy -o setup.json --templates"    
    
    pre_assembly_cmds = list()
    for data in datasets:
        outputdir = os.path.join(tmp_dir,os.path.basename("test_" + data))
        if not(os.path.exists(outputdir)):
            os.mkdir(outputdir)
        pre_assembly_cmds.append("cd " + outputdir + ";" + pre_meta + " " + datasets[data]['options'] + " " + datasets[data]['file'])
    with open(os.path.join(outputdir,"..","cmds"),"w") as f:f.write("\n".join(pre_assembly_cmds)+ "\n")

 
if __name__ == "__main__":
    main()

